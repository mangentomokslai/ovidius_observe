<?php
class Ovidius_Observe_Model_Observer
{
    /* append SKU to product name in product view page */
    public function catalogProductLoadAfter($observer)
    {
        $product = $observer->getEvent()->getProduct();
        $productName = $product->getName();
        $productSku = $product->getSku();
        $productNewName = $productName . ' - ' . $productSku;
        $product->setName($productNewName);
    }

    /* hide product price in price block for not logged in users */
    public function coreBlockAbstractToHtmlBefore($observer)
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()){
            $block = $observer->getBlock();
            if (get_class($block) ==  'Mage_Catalog_Block_Product_Price') {
                $product = $block->getProduct();
                $product->setCanShowPrice(false);
            }
        }
    }
}
